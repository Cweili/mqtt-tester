'use strict';

module.exports = {
  log: {
    level: 'TRACE'
  },
  static: {
    maxAge: 0
  }
};
