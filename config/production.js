'use strict';

module.exports = {
  log: {
    level: 'WARN'
  },
  static: {
    maxAge: 365 * 24 * 60 * 60
  }
};
