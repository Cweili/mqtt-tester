'use strict';

const path = require('path');
const rootPath = path.normalize(__dirname + '/..');
const app = require('../package.json');

module.exports = {
  port: process.env.PORT || 9033,
  dir: {
    root: rootPath,
    frontend: path.join(rootPath, 'app'),
    backend: path.join(rootPath, 'lib'),
    log: path.join(rootPath, 'logs'),
    build: path.join(rootPath, 'build')
  },
  app
};
