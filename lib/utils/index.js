'use strict';

const util = require('util');
const _ = require('lodash');

const promisified = require('./promisified');

const handleError = require('./errorhandler');
const logger = require('./logger');

module.exports = _.assign({}, util, {
  parseJson(json) {
    let obj;
    try {
      obj = JSON.parse(json);
    } catch (e) {
      logger(__filename).error('JSON parse error', e);
    }
    return obj || {};
  },
  pickNotNull() {
    return _.transform(_.pick.apply(_, arguments), (result, value, key) => {
      value != null && (result[key] = value);
    });
  },
  upperCamelCase(string) {
    const firstLetter = string.substr(0, 1).toUpperCase();
    return firstLetter + _.camelCase(string.substr(1));
  },
  toRegExp(str, flags) {
    return new RegExp(_.trim(str).replace(/([\\\/\*\.\+\&\^\[\]\(\)\{\}\|\?\=\!])/g, '\\$1'), flags);
  }
}, {
  handleError,
  logger
}, promisified);
