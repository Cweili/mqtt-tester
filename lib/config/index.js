'use strict';

const _ = require('lodash');
const base = require('../../config/base');
const env = require(`../../config/${process.env.NODE_ENV}`);
const log4js = require('./log4js');
const mosca = require('./mosca');

module.exports = _.merge(base, env, {
  log4js,
  mosca
});
