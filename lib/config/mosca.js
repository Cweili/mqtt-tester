'use strict';

const mosca = require('mosca');
const Utils = require('../utils');
const logger = Utils.logger(__filename);

module.exports = (config) => {
  const options = {
    http: {
      port: config.port,
      static: config.dir.frontend,
      bundle: true
    },
    onlyHttp: true
  };

  let server;

  try {
    server = new mosca.Server(options);
  } catch (err) {
    logger.error(`Mosca error`, err);
  }

  server.on('ready', () => {
    logger.info(`Mosca ready on port ${config.port}`);
  });

  server.on('error', (err) => {
    logger.error(`Mosca error`, err);
  });

  return server;
};
