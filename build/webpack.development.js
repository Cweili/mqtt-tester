'use strict';

const webpack = require('webpack');

const config = require('./webpack.base');

// Add the client which connects to our middleware
// You can use full urls like 'webpack-hot-middleware/client?path=http://localhost:3000/__webpack_hmr'
// useful if you run your app from another point like django
config.entry = config.entry.concat([
  'webpack/hot/dev-server',
  'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000'
]);

config.output.filename = 'app.js';

config.debug = true;
config.devtool = 'source-map';

config.devServer = {
  noInfo: false
};

config.plugins = (config.plugins || []).concat([
  new webpack.HotModuleReplacementPlugin()
]);

module.exports = config;
