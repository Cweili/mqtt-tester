'use strict';

const gulp = require('gulp');

const Config = require('./gulp.config');
const $ = Config.$;

gulp.task('webpack-dev-middleware', (done) => {
  const webpackConfig = require('./webpack.development');
  const compiler = $.webpack(webpackConfig);
  Config.webpackDevMiddleware = [
    $.webpackDevMiddleware(compiler, {
      publicPath: webpackConfig.output.publicPath,
      stats: { colors: true }
    }),
    $.webpackHotMiddleware(compiler, {
      log: $.util.log,
      path: '/__webpack_hmr',
      heartbeat: 10 * 1000
    })
  ];
  done();
});

gulp.task('webpack', (done) => {
  $.webpack(require('./webpack.production'), (err, stats) => {
    if (err) {
      throw new $.util.PluginError('webpack', err);
    }
    $.util.log('[webpack]', stats.toString({ colors: true }));
    done();
  });
});
