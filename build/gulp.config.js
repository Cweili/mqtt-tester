'use strict';

const pkg = require('../package.json');

exports.dir = {
  frontend: 'app',
  backend: 'lib',
  config: 'config',
  log: 'logs',
  dist: 'dist',
  distPkg: `dist/${pkg.name}`
};

exports.$ = {
  browserSync: require('browser-sync').create(),
  del: require('del'),
  eslint: require('gulp-eslint'),
  gzip: require('gulp-gzip'),
  htmlmin: require('gulp-htmlmin'),
  mkdirp: require('mkdirp'),
  nodemon: require('gulp-nodemon'),
  replace: require('gulp-replace'),
  runSequence: require('run-sequence'),
  shell: require('gulp-shell'),
  tar: require('gulp-tar'),
  util: require('gulp-util'),
  webpack: require('webpack'),
  webpackDevMiddleware: require('webpack-dev-middleware'),
  webpackHotMiddleware: require('webpack-hot-middleware')
};

exports.pkg = pkg;
