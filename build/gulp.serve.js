'use strict';

const gulp = require('gulp');

const Config = require('./gulp.config');
const dir = Config.dir;
const $ = Config.$;
const AppBaseConfig = require(`../${dir.config}/base`);

gulp.task('nodemon', ['init'], () => {
  $.nodemon({
    watch: [
      `${dir.backend}/`,
      `${dir.config}/`
    ],
    ext: 'js',
    env: {
      NODE_ENV: 'development'
    }
  })
    .on('restart', () => {
      setTimeout($.browserSync.reload, 1000);
    });
});

gulp.task('browser-sync', ['webpack-dev-middleware'], () => {
  $.browserSync.init({
    proxy: `http://localhost:${AppBaseConfig.port}`,
    browser: 'default',
    middleware: Config.webpackDevMiddleware
  });
});

gulp.task('serve', (done) => {
  $.runSequence(
    'nodemon',
    'browser-sync',
    done
  );
});
