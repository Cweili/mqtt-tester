'use strict';

const fs = require('fs');
const gulp = require('gulp');
const _ = require('lodash');

const Config = require('./gulp.config');
const dir = Config.dir;
const $ = Config.$;

gulp.task('clean', () => {
  return $.del([
    `${dir.dist}/*.*`,
    `${dir.distPkg}/*`,
    `${dir.distPkg}/node_modules`
  ]);
});

gulp.task('html', ['webpack'], () => {
  const webpackConfig = require('./webpack.development');
  const bundleNameDev = webpackConfig.output.filename;
  const bundleName = _.find(
    fs.readdirSync(`${dir.distPkg}/${dir.frontend}`),
    file => new RegExp(`^${bundleNameDev.replace('.', '\\-\\S+?\\.')}\$`).test(file)
  );
  return gulp.src([
    `${dir.frontend}/*.html`
  ])
    .pipe($.replace(bundleNameDev, bundleName))
    .pipe($.replace('@@build.name', Config.pkg.name))
    .pipe($.replace('@@build.version', Config.pkg.version))
    .pipe($.htmlmin({
      collapseWhitespace: true,
      removeAttributeQuotes: true,
      removeComments: true,
      removeCommentsFromCDATA: true,
      removeScriptTypeAttributes: true,
      removeStyleLinkTypeAttributes: true,
      useShortDoctype: true,
      customAttrCollapse: /./,
      customAttrSurround: [
        [/<%/, /%>/]
      ],
      minifyJS: true,
      minifyCSS: true
    }))
    .pipe(gulp.dest(`${dir.distPkg}/${dir.frontend}`));
});

gulp.task('copy', () => {
  return gulp.src([
    `{${dir.backend},${dir.config}}/**/*.*`,
    `!${dir.config}/development.js`,
    Config.pkg.main,
    'package.json'
  ])
    .pipe(gulp.dest(dir.distPkg));
});

// 安装运行依赖的 node_modules
gulp.task('node-modules', ['copy'], $.shell.task([
  `cd ${dir.distPkg} && npm install -d --production`
]));

gulp.task('gzip', ['init', 'html', 'node-modules'], () => {
  return gulp.src(`${dir.distPkg}/**/*`)
    .pipe($.tar(`${Config.pkg.name}-${Config.pkg.version}.tar`))
    .pipe($.gzip())
    .pipe(gulp.dest(dir.dist));
});

gulp.task('building', (done) => {
  $.runSequence(
    'clean',
    'gzip',
    done
  );
});
