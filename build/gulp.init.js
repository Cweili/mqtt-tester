'use strict';

const gulp = require('gulp');

const Config = require('./gulp.config');
const dir = Config.dir;
const $ = Config.$;

gulp.task('mkdir', (done) => {
  $.mkdirp(dir.log, () => {
    $.mkdirp(`${dir.distPkg}/${dir.log}`, done);
  });
});

gulp.task('lint:backend', () => {
  return gulp.src([
    Config.pkg.main,
    `${dir.backend}/**/*.js`,
    `${dir.config}/**/*.js`
  ])
    .pipe($.eslint({ configFile: '.eslintrc' }))
    .pipe($.eslint.format())
    .pipe($.eslint.failAfterError());
});

gulp.task('init', ['mkdir', 'lint:backend']);
