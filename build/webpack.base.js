'use strict';

const path = require('path');
const webpack = require('webpack');

const Config = require('./gulp.config');
const dir = Config.dir;

module.exports = {
  entry: [
    `./${dir.frontend}/index.js`
  ],
  output: {
    path: path.resolve(__dirname, `../${dir.distPkg}/${dir.frontend}`),
    publicPath: '/'
  },
  externals: {
    mqtt: 'mqtt'
  },
  module: {
    loaders: [
      {
        test: /\.vue$/,
        loader: 'vue'
      },
      {
        test: /\.js$/,
        loader: 'babel!eslint',
        // make sure to exclude 3rd party code in node_modules
        exclude: /node_modules/
      },
      {
        // edit this for additional asset file types
        test: /\.(png|jpg|gif)$/,
        loader: 'url',
        query: {
          // inline files smaller then 10kb as base64 dataURL
          limit: 10000,
          // fallback to file-loader with this naming scheme
          name: '[name]-[hash].[ext]'
        }
      },
      {
        test: /\.svg(\?\S*)?$/,
        loader: 'url',
        query: {
          limit: 10000,
          name: '[name]-[hash].[ext]',
          mimetype: 'image/svg+xml'
        }
      },
      {
        test: /\.(woff|woff2|eot|ttf)(\?\S*)?$/,
        loader: 'url',
        query: {
          limit: 10000,
          name: '[name]-[hash].[ext]',
          mimetype: 'application/font-woff'
        }
      }
    ]
  },
  resolve: {
    root: [
      `./${dir.frontend}`,
      './node_modules'
    ],
    extensions: ' .vue .js .styl .css'.split(' ')
  },
  plugins: [
    // new webpack.ProvidePlugin({
    //   Vue: 'vue'
    // }),
    new webpack.optimize.OccurenceOrderPlugin()
  ],
  // vue-loader config:
  // lint all JavaScript inside *.vue files with ESLint
  // make sure to adjust your .eslintrc
  vue: {
    loaders: {
      js: 'babel!eslint'
    },
    autoprefixer: {
      browsers: [
        'last 3 versions',
        'Android > 1',
        'iOS > 4',
        'ie 9',
        '> 1%'
      ]
    },
    html: {
      conservativeCollapse: false
    }
  }
};
