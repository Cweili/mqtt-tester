/**
 * 前端入口
 */
'use strict';

const Vue = require('vue');
const App = require('./app');

Vue.config.debug = true;
Vue.config.silent = !Vue.config.debug;

new Vue({ // eslint-disable-line
  el: 'body',
  components: {
    App
  }
});
