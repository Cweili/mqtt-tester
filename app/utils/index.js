'use strict';

const Vue = require('vue');
const util = Vue.util;

const extend = util.extend;
const toArray = util.toArray;
const isArray = util.isArray;

const utils = {
  noop() {},
  stringify(obj) {
    return JSON.stringify(obj);
  },
  each(collection, handler) {
    return isArray(collection) ? collection.forEach(handler) :
        Object.keys(collection).forEach((key) => handler(collection[key], key));
  },
  pick(object, attrs) {
    const result = {};
    if (!isArray(attrs)) {
      attrs = toArray(arguments).slice(1);
    }
    utils.each(attrs, (attr) => result[attr] = object[attr]);
    return result;
  },
  isString(arg) {
    return typeof arg == 'string';
  }
};

module.exports = extend(utils, util);
